import ballerina/http;

final http:Client clientEndpoint = check new ("http://postman-echo.com");

public function main() returns error? {

    UserDetails[] all_users = [];

    json UserDetails {|
                username: "217121268",
                lastname: "Khama",
                firstname: "Denzel",
                preferred_formats: ["audio", "video", "text"],
                past_subjects: [
                        {
                        "course": "DSA",
                        score: "B+"
                        },
                        {
                        course: "Programming I",
                        score: "A+"
                        }
                    ],
                learning_materials:
    |};
    
    json updatedUserDetails = {|
                username: "errr",
                preferred_formats: ["audio", "video","text"],
                past_subjects: [
                        {
                        "course": "Algorithms",
                        score: "B+"
                        },
                        {
                        "course": "ERP",
                        score: "B+"
                        },
                        {
                        course: "Programming I",
                        score: "A+"
                        },
                        {
                        course: "Mathematics",
                        score: "C+"
                        },
                        {
                        course: "Database",
                        score: "B+"
                        }
                    ]
                |};
                
    json learning_materials =   {
                course: "Distributed Systems Applications",
                learning_objects: {
                required: {
                audio: [
                    {
                        name: "Topic 1",
                        description: "",
                        difficulty: ""
                    }
                ],
                text: [
                    {

                }
                ]
                },
                suggested: {
                        video: [],
                        audio: []
                }
                }
        };
   
    io:println("1. Create a User");
    io:println("2. Update a User");
    io:println("3. Create Learning Material ");

    string input = io:readln("Enter Option: ");
    if (choose === "1"){
        json value = check clientEndpoint->post("/createUserProfile",UserDetails);
        io:println(value);
    }else if (choose === "2") {
       json updatedvalue = check clientEndpoint->post("/updateUserProfile",updatedUserDetails);
       io:println(updatedvalue);
     }else if (choose === "3") {
         json value = check clientEndpoint->post("/createLearningMaterial",learning_materials);
     }
}
