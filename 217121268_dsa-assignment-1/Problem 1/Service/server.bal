import ballerina/io;
import ballerina/http;

//final http:Client clientEndpoint = check new ("http://postman-echo.com");

service / users on new http:Listener(9090) {

//creating a user profile using post
    resource function post insertUserProfile(@http:Payload json UserDetails new_user) returns json {
    io:println(UserDetails.toJsonString()"handling POST request to /users/insert");
    map<json> studentprofile = <map<json>>UserDetails;
    all_users.push(new_user);
    return {UserDetails: "Ok"};

//updating a user profile using post
    resource function post updateUserProfile(@http:Payload json newUserDetails) returns json|error?{

        string message ="";
        map<json> updatedUser = <map<json>>newUserDetails;
        map<json>|error updatefilter = {"username":check updatedUser.username};
        if(updatefilter is error){
            io:println("Error");
        }else{

            if (response > 0 ) {
                io:println("Modified count: '" + response.toString() + "'.") ;
                message = "succesfully Updated User profile!";
            } else {
                io:println("User profile has not been Updated.");
                message = "User profile has not been Updated!";
            }
        }

//returns all users using get
    resource function get all() returns UserDetails[] {
    io:println("handling GET request to /users/all");
    return all_users;
    }

}   

